To create your environment from scratch:
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate active-cells
```

Add the new packages to the `environment.yml` file, and then update your environment with:
```
conda env update -f environment.yml
```

Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name="active-cells"
```

To make the widgets work in JupyterLab:
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib @ryantam626/jupyterlab_code_formatter
jupyter serverextension enable --py jupyterlab_code_formatter
```
