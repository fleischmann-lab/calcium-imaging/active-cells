import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="active-cells",
    version="0.0.1",
    author="Andrea PIERRÉ",
    author_email="andrea_pierre@brown.edu",
    description="Calcium imaging preprocessing package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fleischmann-lab/calcium-imaging/active-cells",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
)
